import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text('Aplikasi Arya'),
          centerTitle: true,
          backgroundColor: Colors.blue,
          leading: new IconButton(icon: new Icon(Icons.web, color: Colors.white), onPressed: () {
          print('Menu');
          },),
          actions: <Widget>[
            new IconButton(icon: new Icon(Icons.thumb_up, color: Colors.white), onPressed: () {
            print('Like');
            },),
            new IconButton(icon: new Icon(Icons.thumb_down, color: Colors.white), onPressed: () {
            print('Dislike');
            },),
          ],
        ),
        body: Center(
          child: Column(children: <Widget>[
            Image.network(
              'https://i.ibb.co/mGnf4Kw/IMG-7175.jpg',
              width: 400.0,
              height: 400.0,
              fit: BoxFit.cover,
            ),
            Text(
              'I Wayan Arya Gina Widyatmaja',
              style:
              TextStyle(fontWeight: FontWeight.bold, height: 2, fontSize: 20),
            ),
            Text(
              '1915051065',
              style:
              TextStyle(fontWeight: FontWeight.bold, height: 2, fontSize: 15),
            ),
          ]),
        ),
      )));
}

